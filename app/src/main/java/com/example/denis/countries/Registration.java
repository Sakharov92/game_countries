package com.example.denis.countries;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class Registration extends AppCompatActivity {
    Button registration;
    EditText logIn, password, country;
    Intent intent;
    ArrayList<Countries> list = new ArrayList<>();
    boolean btnRegistrationEnable = false;
    ArrayList<String> countriesList = new ArrayList<>();
    String userCountry, userRegion;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        sharedPreferences = getSharedPreferences("Users", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        String url = "https://restcountries.eu/rest/v1/all";
        registration = (Button) findViewById(R.id.registration);
        logIn = (EditText) findViewById(R.id.logIn);
        password = (EditText) findViewById(R.id.password);
        country = (EditText) findViewById(R.id.editCountry);
        registration.setEnabled(false);
        password.addTextChangedListener(textWatcher);
        logIn.addTextChangedListener(textWatcher);
        country.addTextChangedListener(textWatcher);
        registration.setOnClickListener(onClickListener);
        logIn.setOnClickListener(onClickListener);
        intent = new Intent(Registration.this, Main.class);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        String name = response.getJSONObject(i).optString("name");
                        String capital = response.getJSONObject(i).optString("capital");
                        String region = response.getJSONObject(i).optString("region");
                        Countries country = new Countries(name, capital, region);
                        countriesList.add(name.toUpperCase());
                        list.add(country);
                        Log.i("Country: ", country.getName() + " - " + country.getCapital() + " - " + country.getRegion());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (i == response.length() - 1) {
                        btnRegistrationEnable = true;
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        Volley.newRequestQueue(Registration.this).add(jsonArrayRequest);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (logIn.length() >= 3 && password.length() >= 5 && country.length() > 0) {
                registration.setEnabled(true);
            } else {
                registration.setEnabled(false);
            }
        }
    };

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.registration:
                    if (logIn.length() > 8) {
                        Toast.makeText(Registration.this, "Имя должно содержать не более 8 символов", Toast.LENGTH_SHORT).show();
                    } else if (password.length() > 8 && btnRegistrationEnable) {
                        Toast.makeText(Registration.this, "Пароль должен содержать не более 8 символов", Toast.LENGTH_SHORT).show();
                    } else {
                        userCountry = country.getText().toString().toUpperCase();
                        if (btnRegistrationEnable && countriesList.contains(userCountry)) {
                            for (Countries c : list) {
                                if (c.getName().equalsIgnoreCase(userCountry)) {
                                    userRegion = c.getRegion();
                                    break;
                                }
                            }
                            intent.putExtra("name", userCountry);
                            intent.putExtra("region", userRegion);
                            intent.putExtra("logIn", logIn.getText().toString().toLowerCase());
                            editor.putString(logIn.getText().toString().toLowerCase() + "userRegion", userRegion);
                            editor.putString(logIn.getText().toString().toLowerCase(), password.getText().toString().toLowerCase());
                            editor.putInt(logIn.getText().toString().toLowerCase() + "scoreTrue", 0);
                            editor.putInt(logIn.getText().toString().toLowerCase() + "scoreFalse", 0);
                            startActivity(intent);
                            editor.commit();
                        } else {
                            Toast.makeText(Registration.this, "Такой страны не существует", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case R.id.btn_signIn:
                    Intent intent2 = new Intent(Registration.this, LogIn.class);
                    startActivity(intent2);
                    break;
            }
        }
    };
}
