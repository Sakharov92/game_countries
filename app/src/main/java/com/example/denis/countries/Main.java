package com.example.denis.countries;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class Main extends AppCompatActivity {
    static ArrayList<Countries> list = new ArrayList<>();
    String userCountry, userRegion;
    TextView question;
    Button button;
    EditText answer;
    ProgressBar progressBar;
    int numberOfQuestion, rightAnswers, falseAnswers;
    int lotOfCountries = 0;
    String logIn;
    String url = "https://restcountries.eu/rest/v1/all";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        question = (TextView) findViewById(R.id.quastion);
        answer = (EditText) findViewById(R.id.answer);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(onClickListener);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        Intent intent = getIntent();
        userCountry = intent.getStringExtra("name");
        userRegion = intent.getStringExtra("region");
        logIn = intent.getStringExtra("logIn");
        sharedPreferences = getSharedPreferences("Users", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        rightAnswers = sharedPreferences.getInt(logIn.toLowerCase() + "scoreTrue", -1);
        falseAnswers = sharedPreferences.getInt(logIn.toLowerCase() + "scoreFalse", -1);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, ResponseListner, ResponseErrorListner);
        Volley.newRequestQueue(Main.this).add(jsonArrayRequest);
    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.button:
                    if (list.size() <= 1) {
                        Toast.makeText(Main.this, "Game Over", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Main.this, Score.class);
                       // intent.putExtra("rightAnswers", rightAnswers);
                       // intent.putExtra("falseAnswers", falseAnswers);
                        intent.putExtra("logIn", logIn);
                        startActivity(intent);
                    } else {
                        if (answer.getText().toString().equalsIgnoreCase(list.get(numberOfQuestion).getCapital())) {
                            //  Toast.makeText(Main.this, "Right", Toast.LENGTH_SHORT).show();
                            ++rightAnswers;
                            list.remove(numberOfQuestion);
                            --lotOfCountries;
                            answer.setText("");
                            numberOfQuestion = (int) (Math.random() * lotOfCountries);
                            question.setText("What is the capital of " + list.get(numberOfQuestion).getName() + "?");
                            editor.putInt(logIn + "scoreTrue", rightAnswers);
                            editor.putInt(logIn + "scoreFalse", falseAnswers);
                            editor.commit();
                        } else {
                            //    Toast.makeText(Main.this, "False", Toast.LENGTH_SHORT).show();
                            ++falseAnswers;
                            list.remove(numberOfQuestion);
                            --lotOfCountries;
                            answer.setText("");
                            numberOfQuestion = (int) (Math.random() * lotOfCountries);
                            question.setText("What is the capital of " + list.get(numberOfQuestion).getName() + "?");
                            editor.putInt(logIn + "scoreTrue", rightAnswers);
                            editor.putInt(logIn + "scoreFalse", falseAnswers);
                            editor.commit();
                         }
                    }
            }
        }
    };
    Response.Listener<JSONArray> ResponseListner = new Response.Listener<JSONArray>() {
        @Override
        public void onResponse(JSONArray response) {
            for (int i = 0; i < response.length(); i++) {
                try {
                    String region = response.getJSONObject(i).optString("region");
                    if (region.equalsIgnoreCase(userRegion)) {
                        Gson gson = new Gson();
                        Countries countries = gson.fromJson(String.valueOf(response.getJSONObject(i)), Countries.class);
                       // Log.i("!!!", countries.getName() + " " + countries.getRegion() + " " + countries.getCapital());
                        list.add(countries);
                        lotOfCountries++;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            numberOfQuestion = (int) (Math.random() * lotOfCountries);
            progressBar.setVisibility(View.GONE);
            question.setText("What is the capital of " + list.get(numberOfQuestion).getName() + "?");
            answer.setVisibility(View.VISIBLE);
            answer.setText("");
            button.setVisibility(View.VISIBLE);
            button.setEnabled(true);
        }
    };
    Response.ErrorListener ResponseErrorListner = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
        }
    };
}