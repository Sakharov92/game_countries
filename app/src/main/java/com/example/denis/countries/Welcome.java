package com.example.denis.countries;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Welcome extends AppCompatActivity {
    Button btn_registration, btn_signIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        btn_registration = (Button) findViewById(R.id.btn_registration);
        btn_signIn = (Button) findViewById(R.id.btn_signIn);
        btn_registration.setOnClickListener(onClickListener);
        btn_signIn.setOnClickListener(onClickListener);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_registration:
                    Intent intent = new Intent(Welcome.this, Registration.class);
                    startActivity(intent);
                    break;
                case R.id.btn_signIn:
                    Intent intent2 = new Intent(Welcome.this,LogIn.class);
                    startActivity(intent2);
            }
        }
    };
}
