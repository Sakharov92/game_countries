package com.example.denis.countries;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Score extends AppCompatActivity {
    String logIn;
    TextView right, falsE, userName;
    int rightAnswers, falseAnswers;
    Button finish;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        right = (TextView) findViewById(R.id.right);
        falsE = (TextView) findViewById(R.id.falsE);
        userName = (TextView) findViewById(R.id.userName);
        finish = (Button) findViewById(R.id.button);
        final Intent intent = getIntent();
        logIn = intent.getStringExtra("logIn");
        sharedPreferences = getSharedPreferences("Users", Context.MODE_PRIVATE);

        rightAnswers = sharedPreferences.getInt(logIn + "scoreTrue", 0);
        falseAnswers = sharedPreferences.getInt(logIn + "scoreFalse", 0);
        right.setText(String.valueOf(rightAnswers));
        falsE.setText(String.valueOf(falseAnswers));
        userName.setText(logIn);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Score.this, Welcome.class);
                startActivity(intent);
            }
        });
    }
}
