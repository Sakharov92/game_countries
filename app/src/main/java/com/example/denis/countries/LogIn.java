package com.example.denis.countries;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LogIn extends AppCompatActivity {
    String userCountry;
    EditText login, password;
    Button btn_logIn;
    SharedPreferences sp;
    int scoreRight, scoreFalse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        sp = getSharedPreferences("Users", Context.MODE_PRIVATE);
        login = (EditText) findViewById(R.id.login2);
        password = (EditText) findViewById(R.id.password2);
        btn_logIn = (Button) findViewById(R.id.signIn);
        btn_logIn.setOnClickListener(onClickListener);
        login.addTextChangedListener(textListner);
        password.addTextChangedListener(textListner);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String pass = sp.getString(login.getText().toString().toLowerCase(), null);
            String region = sp.getString(login.getText().toString().toLowerCase() + "userRegion", null);

            if (password.getText().toString().equalsIgnoreCase(pass) && region != null && pass != null) {
                Intent intent = new Intent(LogIn.this, Main.class);
                intent.putExtra("name", userCountry);
                intent.putExtra("region", region);
                intent.putExtra("logIn", login.getText().toString());
                startActivity(intent);
            } else {
                Toast.makeText(LogIn.this, "Логин или пароль введены не верно", Toast.LENGTH_SHORT).show();
            }
        }
    };

    TextListner textListner = new TextListner() {
        @Override
        public void afterTextChanged(Editable editable) {
            if (login.getText().toString().equals("") || password.getText().toString().equals("")) {
                btn_logIn.setEnabled(false);
            } else {
                btn_logIn.setEnabled(true);
            }
        }
    };

    abstract class TextListner implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
    }
}
